import string
from random import SystemRandom

cryptogen = SystemRandom()

min_digits_count = 4
digits = string.digits


__all__ = ["generate_activation_code"]


def generate_activation_code(digits_count: int = min_digits_count) -> str:
    return "".join(
        cryptogen.choice(digits) for _ in range(max(min_digits_count, digits_count))
    )
