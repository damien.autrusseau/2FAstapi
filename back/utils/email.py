import os
import aiosmtplib
from urllib.parse import urlparse
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import ssl


SMTP_URL: str = os.getenv("SMTP_URL", "smtp://smtp:1025")
SMTP = urlparse(SMTP_URL)

__all__ = ["send_email"]


async def send_email(
    sender: str = "",
    to: list[str] = [],
    cc: list[str] = [],
    bcc: list[str] = [],
    subject: str = "",
    content: str = "",
):
    """
    Sends an email to a list of recipients.

    Parameters:
        sender (str): The email address of the sender.
        to (list): A list of email addresses.
        cc (list): A list of email addresses.
        bcc (list): A list of email addresses.
        subject (str): The subject of the email.
        content (str): The content of the email.

    Returns:
        None
    """
    msg = MIMEMultipart()
    msg.preamble = subject
    msg["Subject"] = subject
    msg["From"] = sender
    msg["To"] = ", ".join(to)
    if len(cc):
        msg["Cc"] = ", ".join(cc)
    if len(bcc):
        msg["Bcc"] = ", ".join(bcc)

    msg.attach(MIMEText(content, "plain", "utf-8"))

    use_tls = SMTP.scheme == "smtps"
    smtp = aiosmtplib.SMTP(
        hostname=SMTP.hostname,
        port=SMTP.port,
        use_tls=use_tls,
        validate_certs=not use_tls,
        tls_context=ssl._create_unverified_context() if not use_tls else None,
    )
    await smtp.connect()
    if use_tls:
        await smtp.starttls()
    if SMTP.username:
        await smtp.login(SMTP.username, SMTP.password)
    await smtp.send_message(msg)
    await smtp.quit()
    return True
