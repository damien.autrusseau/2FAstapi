import logging
from fastapi.middleware.cors import CORSMiddleware
from fastapi import FastAPI
from api.v1.routes import router as router_v1
from conf import HOSTNAME


logger = logging.getLogger(__name__)

app = FastAPI(
    version=1.0,
    openapi_tags=[
        {
            "name": "account",
            "description": "User, registration, TFA",
        },
    ],
    docs_url="/api/v1/docs",
    openapi_url="/api/v1/openapi.json",
)


app.include_router(router_v1, prefix="/api/v1")

app.add_middleware(
    CORSMiddleware,
    allow_origins=[HOSTNAME],
    allow_credentials=True,
    allow_methods=[HOSTNAME],
    allow_headers=[HOSTNAME],
)
