from api.v1 import APIRouter, Request, ModelInput
from db import models, connect_db
from fastapi import HTTPException
from pydantic import UUID4
from api.v1.schemas import UserOutput

router = APIRouter()


class ActivateBody(ModelInput):
    connexion_request_uuid: UUID4
    activation_code: str


@router.post("/activate")
async def activate(
    request: Request,
    body: ActivateBody,
) -> UserOutput:
    """
    Get/create + actiavte a user from a connexion request (uuid + activation code)

    Args:
        request (Request): The request object
        body (ActivateBody): Requirements for retrieving the connexion_request

    Raises:
        HTTPException: 400 Activation code expired
        HTTPException: 404 Invalid activation code

    Returns:
        UserOutput: Registered user
    """
    async with connect_db() as session:
        # Retrieve the connexion request from uuid + code
        connexion_request = (
            session.query(models.ConnexionRequest)
            .filter(
                models.ConnexionRequest.uuid == body.connexion_request_uuid,
                models.ConnexionRequest.activation_code == body.activation_code,
            )
            .first()
        )

        if connexion_request:
            if connexion_request.is_expired:
                # Burn the connexion request
                await connexion_request.burn(session)

                raise HTTPException(status_code=400, detail="Activation code expired")

            # At this point the user is verified, ready for activation
            # Get or create the user from the connexion request
            user = await models.User.get_or_create_from_connexion_request(
                connexion_request, session
            )

            # Burn the connexion request
            await connexion_request.burn(session)

            # Set the user for the session requests
            request.set_user(user)

            session.commit()
            session.refresh(user)
            return user

    # Any other case is an error
    raise HTTPException(status_code=404, detail="Invalid activation code")
