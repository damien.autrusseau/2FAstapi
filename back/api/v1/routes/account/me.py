from api.v1 import APIRouter, Request
from api.v1.schemas import UserOutput

router = APIRouter()


@router.get("/me")
async def request(request: Request) -> UserOutput:
    """Just return the current user model attached to the request

    Args:
        request (Request): the request

    Returns:
        UserOutput: the current user model as response
    """
    return request.user
