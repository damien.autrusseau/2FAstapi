from api.v1 import APIRouter, Request
from db import connect_db
import secrets

from fastapi import HTTPException

router = APIRouter()


@router.get("/logout")
async def logout(request: Request) -> None:
    """
    Logout endpoint

    Args:
        request (Request): request object

    Raises:
        HTTPException: 401 Not authenticated

    Returns:
        User: the disconnected user
    """
    if request.user.is_authenticated:
        async with connect_db() as session:
            # Logout from other session, other browser, all devices by changing the session token
            request.user.session_token = secrets.token_hex()
            session.commit()
        # clear cookies
        request.cookies.clear()
        request.set_user(None)
        return None
    raise HTTPException(status_code=401, detail="Not authenticated")
