from api.v1 import APIRouter
from . import me, register, activate, logout

router = APIRouter()
router.include_router(me.router)
router.include_router(register.router)
router.include_router(activate.router)
router.include_router(logout.router)
