from pydantic import Field, EmailStr, SecretStr, validator
from fastapi import HTTPException
from api.v1 import APIRouter, Request, ModelInput
from db import models, connect_db
from utils import hash_password
from api.v1.schemas import ConnexionRequestOutput

router = APIRouter()


class RegisterBody(ModelInput):
    email: EmailStr = Field(
        description="Email of the user",
        example="damien.autrusseau@surviving-data.fr",
    )
    password: SecretStr = Field(description="Password of the user", example="123456")

    @validator("password", always=True)
    def validate_password(cls, value: SecretStr):
        """
        Validate the password

        Args:
            value (SecretStr): password

        Raises:
            ValueError: 8 characters min

        Returns:
            str: password
        """
        password = value.get_secret_value()
        if len(password) < 8:
            raise ValueError("The password must be at least 8 characters")
        return password


@router.post("/register")
async def register(
    request: Request,
    body: RegisterBody,
) -> ConnexionRequestOutput:
    """
    Register endpoint, needs email and password to authenticate

    Args:
        request (Request): the request object
        body (RegisterBody):  the required payload

    Raises:
        HTTPException: 404 Already logged in

    Returns:
        ConnexionRequestOutput: _description_
    """
    if request.user.is_authenticated:
        raise HTTPException(status_code=404, detail="User already logged in")

    hashed_password = hash_password(body.password)
    connexion_request = models.ConnexionRequest(
        email=body.email, hashed_password=hashed_password
    )
    async with connect_db() as session:
        session.add(connexion_request)
        session.commit()
        await connexion_request.send_email()

    return connexion_request
