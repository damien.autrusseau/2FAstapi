from api.v1 import APIRouter
from . import account

router = APIRouter()
router.include_router(account.router, prefix="/account", tags=["account"])
