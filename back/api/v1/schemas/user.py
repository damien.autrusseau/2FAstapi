import uuid
from api.v1.models import Model, ModelOutput

__all__ = ["UserOutput"]


class UserBase(Model):
    email: str | None  # anonymous case
    is_authenticated: bool = False


class UserOutput(UserBase, ModelOutput):
    uuid: uuid.UUID
