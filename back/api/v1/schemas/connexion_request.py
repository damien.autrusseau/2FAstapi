from datetime import datetime
import uuid
from api.v1.models import Model, ModelOutput
from pydantic import EmailStr

__all__ = ["ConnexionRequestOutput"]


class ConnexionRequestBase(Model):
    uuid: uuid.UUID
    email: EmailStr
    created_at: datetime


class ConnexionRequestOutput(ConnexionRequestBase, ModelOutput):
    uuid: uuid.UUID
