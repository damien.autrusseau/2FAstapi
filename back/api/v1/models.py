from pydantic import BaseModel


def to_camel_case(string: str) -> str:
    """
    Convert string to Camel Case
    """
    components = string.split("_")
    return components[0] + "".join(x.title() for x in components[1:])


class Model(BaseModel):
    """
    Generic schema model
    """

    class Config:
        allow_population_by_field_name = True
        alias_generator = to_camel_case
        extra = "forbid"


class ModelOutput(Model):
    """
    Generic schema model output
    """

    class Config:
        orm_mode = True


class ModelInput(Model):
    """
    Generic schema model  input
    """

    pass
