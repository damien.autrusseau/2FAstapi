from fastapi import Request as BaseRequest, Response
from db import connect_db_sync, connect_db, models


class Request(BaseRequest):
    """
    This sets up a custom request handler for the Request class.
    """

    _user: models.User

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.set_user()

    def get_user(self) -> models.User:
        """
        Get the user from the current (or not) sessionToken cookie or return an anonymous user

        Returns:
            User: the request user
        """
        user: models.User | None = None
        session_token = self.cookies.get("sessionToken")
        if session_token:
            with connect_db_sync() as session:
                user = (
                    session.query(models.User)
                    .filter(models.User.session_token == session_token)
                    .first()
                )
        return user or models.User.get_anonymous()

    def set_user(self, user: models.User | None = None) -> None:
        """
        Forced the user attached to the request

        Args:
            user (User | None, optional): optionnal forced user
        """
        if user:
            self._user = user
        else:
            self._user = self.get_user()

    def sign_response(self, response: Response) -> None:
        """
        Sign the response with the current user sessionToken

        Args:
            response (Response): the server response instance
        """
        response.set_cookie(key="sessionToken", value=self._user.session_token)

    @property
    def user(self) -> models.User:
        """
        # Override starlette pre-defined user

        Returns:
            User: the user instance
        """
        return self._user

    @property
    async def db(self):
        """
        Async context manager for the database session

         Yields:
             Session: async db session
        """
        async with connect_db() as session:
            yield session
