from datetime import timedelta
from fastapi.testclient import TestClient

from api import app

client = TestClient(app)

email_ok = "ok@ok.fr"
password_ok = "password1234"


def test_me_anoymous(is_valid_uuid4):
    """
    Verify anonymous user informations
    """
    response = client.get("/api/v1/account/me")
    assert response.status_code == 200
    data = response.json()
    assert data["email"] is None
    assert data["isAuthenticated"] is False
    assert is_valid_uuid4(data["uuid"])


def test_logout_anonymous():
    """
    Can't logout if not logged in
    """
    response = client.get("/api/v1/account/logout")
    assert response.status_code == 401


def test_register_imcomplete():
    response = client.post("/api/v1/account/register", json={"email": email_ok})
    assert response.status_code == 422
    response = client.post("/api/v1/account/register", json={"password": password_ok})
    assert response.status_code == 422


def test_register_wrong_data():
    response = client.post(
        "/api/v1/account/register",
        json={"email": "wrong@wrong", "password": password_ok},
    )
    assert response.status_code == 422
    response = client.post(
        "/api/v1/account/register",
        json={"email": email_ok, "password": "2short"},
    )
    assert response.status_code == 422


def test_register_ok(register_response_ok, is_valid_uuid4):
    data = register_response_ok.json()
    assert data["email"] == "ok@ok.fr"
    assert is_valid_uuid4(data["uuid"])


def test_activate_invalid_code(register_response_ok):
    data = register_response_ok.json()
    response = client.post(
        "/api/v1/account/activate",
        json={
            "connexion_request_uuid": data["uuid"],
            "activation_code": "345",
        },
    )
    assert response.status_code == 404
    data = response.json()
    assert data["detail"] == "Invalid activation code"


def test_activate_expired_code(
    register_response_ok, last_connexion_request, db_session
):
    data = register_response_ok.json()

    last_connexion_request.created_at = last_connexion_request.created_at - timedelta(
        minutes=2
    )
    db_session.commit()
    db_session.refresh(last_connexion_request)

    response = client.post(
        "/api/v1/account/activate",
        json={
            "connexion_request_uuid": data["uuid"],
            "activation_code": last_connexion_request.activation_code,
        },
    )
    assert response.status_code == 400
    data = response.json()
    assert data["detail"] == "Activation code expired"


def test_activate_ok(
    register_response_ok, last_connexion_request, last_created_user, is_valid_uuid4
):
    data = register_response_ok.json()

    response = client.post(
        "/api/v1/account/activate",
        json={
            "connexion_request_uuid": data["uuid"],
            "activation_code": last_connexion_request.activation_code,
        },
    )
    assert response.status_code == 200
    data = response.json()
    assert is_valid_uuid4(data["uuid"])

    # Verify the user is active
    assert last_created_user.is_active is True
    # Verify the user has the new session token
    # assert (
    #     response.headers["set-cookie"]
    #     == f"sessionToken={last_created_user.session_token}; Path=/; SameSite=lax"
    # )
