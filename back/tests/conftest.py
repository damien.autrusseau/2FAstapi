import datetime
from typing import Iterator
from fastapi.testclient import TestClient
from api import app
import pytest
import uuid
from db import models, connect_db_sync, Session

client = TestClient(app)

email_ok: str = "ok@ok.fr"
password_ok: str = "OKFORSECURITY1234"


@pytest.fixture()
def is_valid_uuid4():
    def wrapper(value):
        try:
            return value == str(uuid.UUID(value, version=4))
        except ValueError:
            return False

    return wrapper


@pytest.fixture
def register_response_ok():
    """_summary_

    Returns:
        _type_: _description_
    """
    response = client.post(
        "/api/v1/account/register",
        json={"email": email_ok, "password": password_ok},
    )
    assert response.status_code == 200
    return response


@pytest.fixture
def db_session() -> Session:
    """_summary_

    Returns:
        Session: _description_

    Yields:
        Iterator[Session]: _description_
    """
    with connect_db_sync() as session:
        yield session


@pytest.fixture
def last_connexion_request(db_session) -> models.ConnexionRequest:
    """
    Retrieve the last connexion request to simulate the email receiving

    Args:
        db_session (Session): DB Session

    Returns:
        ConnexionRequest: instance of the last inserted connexion_request
    """
    connexion_request = (
        db_session.query(models.ConnexionRequest)
        .order_by(models.ConnexionRequest.created_at.desc())
        .first()
    )
    return connexion_request


@pytest.fixture
def last_created_user(db_session) -> models.User:
    user = db_session.query(models.User).order_by(models.User.created_at.desc()).first()
    return user
