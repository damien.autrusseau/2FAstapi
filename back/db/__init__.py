import os
import sqlalchemy
from typing import AsyncIterator, Iterator
from sqlalchemy import create_engine
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm import Session
from contextlib import asynccontextmanager, contextmanager


__all__ = ["get_db", "sync_get_db", "Model", "Session"]

DATABASE_URL = os.getenv(
    "POSTGRES_URL",
    "postgresql://postgres:postgres@postgres:5432/postgres?sslmode=disable",
)

metadata = sqlalchemy.MetaData()
engine = create_engine(
    DATABASE_URL,
    pool_pre_ping=True,
)
metadata.create_all(engine)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


class Model(DeclarativeBase):
    pass


class DBContextManager:
    """
    DB Context Manager
    """

    session: Session

    def __init__(self):
        self.session = SessionLocal()

    async def __call__(self) -> AsyncIterator[Session]:
        yield self.session

    def __enter__(self):
        return self.session

    def __exit__(self, exc_type, exc_value, traceback):
        """
        Close the session connection at the end of the context

        Args:
            exc_type (_type_): exception type
            exc_value (_type_): exception value
            traceback (_type_): exception traceback
        """
        self.session.flush()
        self.session.close()


@asynccontextmanager
async def connect_db() -> AsyncIterator[Session]:
    """
    Get an async db connection for limited context

    Returns:
        Session: a DB Session

    Yields:
        AsyncIterator[Session]: _description_
    """
    with DBContextManager() as session:
        yield session


@contextmanager
def connect_db_sync() -> Iterator[Session]:
    """
    Get an sync db connection for limited context

    Returns:
        Session: a DB Session

    Yields:
        Iterator[Session]: _description_
    """
    with DBContextManager() as session:
        yield session
