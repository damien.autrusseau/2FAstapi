from datetime import datetime, timedelta
from sqlalchemy import DateTime, Column, String
from sqlalchemy.sql import func
from sqlalchemy.dialects.postgresql import UUID
from utils.email import send_email
from db import Model, Session
from utils import generate_activation_code
from uuid import uuid4

from conf import HOSTNAME, CONNEXION_REQUEST_EXPIRATION_MINUTES

__all__ = ["ConnexionRequest"]


def default_activation_code() -> str:
    return generate_activation_code(4)


class ConnexionRequest(Model):
    __tablename__ = "connexion_requests"

    uuid: str = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid4)
    created_at: datetime = Column(DateTime(), default=func.now())
    email: str = Column(String, index=True)
    hashed_password: str = Column(String)
    activation_code: str = Column(String, default=default_activation_code, index=True)

    @property
    def is_expired(self) -> bool:
        return datetime.now() > self.created_at + timedelta(
            minutes=CONNEXION_REQUEST_EXPIRATION_MINUTES
        )

    async def send_email(self) -> bool:
        """
        Send an email to the user with the activation code
        """
        return await send_email(
            sender=f"2fa@${HOSTNAME}",
            to=[self.email],
            subject="Complete your registration",
            content=f"""<p>
                Activate your account, input this activation code on the registration screen:
                <b>{self.activation_code}</b>
            </p>""",
        )

    async def burn(self, session: "Session") -> None:
        """
        Remove the connexion request
        """
        session.delete(self)
