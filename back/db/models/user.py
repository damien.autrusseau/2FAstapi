import uuid
from sqlalchemy import Boolean, Column, String, DateTime, Column, String
from sqlalchemy.dialects.postgresql import UUID
from datetime import datetime
from sqlalchemy.sql import func
from db import Model, Session, models
import uuid
import secrets

__all__ = ["User"]


class User(Model):
    __tablename__ = "users"

    uuid: str = Column(
        UUID(as_uuid=True), primary_key=True, index=True, default=uuid.uuid4
    )
    created_at: datetime = Column(DateTime(), default=func.now())
    email: str = Column(String, unique=True, index=True)
    hashed_password: str = Column(String)
    is_active: bool = Column(Boolean, default=True)
    session_token: str = Column(String, default=secrets.token_hex)

    is_anonymous = False

    @property
    def is_authenticated(self) -> bool:
        return not self.is_anonymous

    @classmethod
    async def get_or_create_from_connexion_request(
        cls, connexion_request: "models.ConnexionRequest", session: "Session"
    ) -> "User":
        user = session.query(User).filter(User.email == connexion_request.email).first()
        if not user:
            user = User(
                email=connexion_request.email,
                hashed_password=connexion_request.hashed_password,
            )
            session.add(user)

        user.is_active = True

        # Change the session token
        user.session_token = secrets.token_hex()

        return user

    @classmethod
    def get_anonymous(cls) -> "User":
        """instanciate an anonymous user

        Returns:
            User: _description_
        """
        return cls(uuid=uuid.uuid4(), email=None, is_anonymous=True)
