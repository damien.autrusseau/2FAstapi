import os


HOSTNAME: str = os.getenv("HOSTNAME", "2fa.localhost")
CONNEXION_REQUEST_EXPIRATION_MINUTES: int = 1
