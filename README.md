# 2FAstapi

This project is a proof of skills for the following subject : **Building a user registration API**

## Context

As a core API developer, you are responsible for building this feature and expose it through API.

## Specifications

You have to manage a user registration and his activation.

The API must support the following use cases:

-   Create a user with an email and a password.
-   Send an email to the user with a 4 digits code.
-   Activate this account with the 4 digits code received. For this step, we consider a `BASIC AUTH` is enough to check if he is the right user.
-   The user has only one minute to use this code. After that, an error should be raised.

Design and build this API. You are completely free to propose the architecture you want.

## What do we expect?

-   Your application should be in Python.
-   We expect to have a level of code quality which could go to production.
-   Using frameworks is allowed only for routing, dependency injection, event dispatcher, db connection. Don't use magic (ORM for example)! We want to see **your** implementation.
-   Use the DBMS you want (except SQLite).
-   Consider the SMTP server as a third party service offering an HTTP API. You can mock the call, use a local SMTP server running in a container, or simply print the 4 digits in console. But do not forget in your implementation that **it is a third party service**.
-   Your code should be tested.
-   Your application has to run within a docker containers.
-   You should provide us the source code (or a link to GitHub)
-   You should provide us the instructions to run your code and your tests. We should not install anything except docker/docker-compose to run you project.
-   You should provide us an architecture schema.

# Requirements

-   a bash terminal
-   a recent docker + compose version
-   a recent web browser (chrome like)
-   internet connexion

# Getting started

Clone the project

```bash
git clone git@gitlab.com:damien.autrusseau/2FAstapi.git
cd 2FAstapi
```

Be sure your port 80 is free, or [set a new one into your .env](#customize-your-env)

```bash
./2fa start
```

# Running tests

```bash
./2fa test
```

then follow indications on terminal, or open http://2fa.localhost

# CLI usage

```bash
./2fa test # Launch the tests
./2fa logs # Launch a docker following output for the whole project containers
./2fa logs api # Launch a docker following output for the api container
./2fa logs [...options] # Launch a docker following output with any options
./2fa poetry install # Install the dependencies
./2fa poetry add fastapi # Add a new fastapi depende,ncy
./2fa poetry [...options] # Launch poetry with any options
./2fa compose run --rm api bash # Launch a bash terminal into the api container
./2fa compose [...options] # Launch docker-compose with any options
./2fa db reset # Reset the database (alembic)
./2fa db upgrade # Upgrade the database # Install the dependencies
./2fa db revise # Create a new database revision # Install the dependencies
./2fa db psql # Open a psql terminal into the database container
./2fa stop # Stop the project
./2fa kill # Force kill the project
```

# Customize your .env

```bash
#.env
PROJECT=2fa
HOSTNAME=2fa.localhost
PORT=80
```

# The solution explained

## > Create a user with an email and a password.

I choose to start from a well structured project that can be easily extended and maintained, with a first version iteration delivered on `/api/v1/`

The user fetch `/api/v1/account/me` from the web view to retrieve his current informations (already logged in or not)
I use sessionToken in cookie for this purpose, but it can be easily replaced by a JWT token or any other authentication system.

The user fetch API at `/api/v1/account/register` through a web form with his email and his password

A `ConnexionRequest` db row is created with a random 4 digits code and the regarding user informations

I choose to create a `ConnexionRequest` instead of a `User` directly row because it's more secure to store a non related Entity than modify the potential existing `User` informations.

**Remember the case of 2 different peoples trying to connect with the same email at the same time, overriding the same `User` instance may cause issues for one of them, but we can't know the right is.**

The `ConnexionRequest` contains the following informations :

-   created_at
-   email
-   hashed_password
-   activation_code

## > Send an email to the user with a 4 digits code.

A email was sent to the user with a 4 digits code inside from the `ConnexionRequest`, just right after `ConnexionRequest` is created

## > Activate this account with the 4 digits code received

The user fetch API at `/api/v1/account/activate` throw a web form with the uuid and the activation_code of the `ConnexionRequest`
If a `ConnexionRequest` is found, the user is created or updated with password, email and `is_active=True`.
if not found => HTTPException: 404 Invalid activation code
the connexion request is burn and I attach the session token in cookies
A a new page load, the session token is sent to each request and the user is logged in because i retrieve `User` for `session_token`

## > The user has only one minute to use this code. After that, an error should be raised.

On `/api/v1/account/activate` i also check the ConnexionRequest validity with `connexion_request.is_expired`
if expired => HTTPException: 400 Activation code expired

# The solution potential improvements

We can expect the next login after a successful workflow complete may implements the classic login workflow with email and password.
We just have to test if the user already exists with the email and test if the password match on `/api/v1/account/register`

# Documents

## Workflow demo

![Workflow demo](./docs/WorkflowDemo.webm)

## Architecture schema

<img src="./docs/ArchitectureServices.svg" data-canonical-src="./docs/ArchitectureServices.svg" width="100%"/>

## Workflow schema

<img src="./docs/WorkflowSchema.svg" data-canonical-src="./docs/WorkflowSchema.svg" width="100%"/>
