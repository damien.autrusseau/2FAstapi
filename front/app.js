const {
    Box,
    Modal,
    Toolbar,
    CardHeader,
    Alert,
    Typography,
    AppBar,
    CssBaseline,
    createTheme,
    colors,
    ThemeProvider,
    Card,
    CardContent,
    CardActions,
    Button,
    TextField,
} = MaterialUI

const theme = createTheme({
    palette: {
        primary: {
            main: '#2d2d2d',
        },
        secondary: {
            main: '#19857b',
        },
        error: {
            main: colors.red.A400,
        },
    },
})
const api = new (class {
    constructor() {
        this.base = '/api/v1'
    }

    async get(route, options = {}) {
        return await this.request(route, { ...options, method: 'GET' })
    }

    async post(route, options = {}) {
        return await this.request(route, { ...options, method: 'POST' })
    }

    async request(route, { method, data, params } = {}) {
        try {
            return await axios({
                url: `/api/v1/${route}`,
                method: method || 'GET',
                params,
                data,
                headers: {
                    'Content-Type': 'application/json',
                },
            })
        } catch (error) {
            return { errors: this.transformErrors(error) }
        }
    }
    transformErrors(error) {
        const transformedErrors = {}
        if (error && error.response && error.response.data && error.response.data.detail) {
            const detail = error.response.data.detail
            if (detail instanceof Array) {
                detail.forEach((error) => {
                    const field = error.loc.slice(1).join('.')
                    const message = error.msg
                    transformedErrors[field] = message
                })
            } else {
                transformedErrors['global'] = detail
            }
        }
        return transformedErrors
    }
})()

function ActivateForm({ connexionRequest: { uuid, email } }) {
    const [seconds, setSeconds] = React.useState(60)
    const [activationCode, setActivationCode] = React.useState('')
    const [errors, setErrors] = React.useState({})
    const handleChange = async (e) => {
        const code = e.target.value.substring(0, 4)
        setActivationCode(code)
        const res = await api.post('account/activate', {
            data: {
                connexionRequestUuid: uuid,
                activationCode: code,
            },
        })
        res.errors && setErrors(res.errors)
        if (res.data) {
            window.location.reload()
        }
        // res.data && setConnexionRequest(res.data)
    }
    const percent = (seconds * 100) / 60
    React.useEffect(() => {
        const interval = setInterval(() => {
            setSeconds((prev) => Math.max(0, prev - 1))
        }, 1000)
        return () => {
            clearInterval(interval)
        }
    }, [])
    return (
        <form
            style={{
                display: 'flex',
                flexDirection: 'column',
                gap: '1rem',
                padding: '1rem',
            }}
        >
            <CardHeader title="Complete the registration" />
            {activationCode.length >= 4 && errors.global && (
                <Alert severity="error">{errors.global}</Alert>
            )}
            <TextField
                helperText={`Input the activation code sent at ${email}`}
                type="number"
                value={activationCode}
                label="Activation code"
                onChange={handleChange}
                required
            />
            {seconds > 0 ? `${seconds} seconds left` : 'expired'}
            <Box
                sx={{
                    outline: '1px solid rgba(0, 0, 0, 0.23)',
                    borderRadius: '0.2rem',
                    overflow: 'hidden',
                }}
            >
                <Box
                    sx={{
                        background: colors.red.A400,
                        width: `${percent}%`,
                        height: '1rem',
                    }}
                ></Box>
            </Box>
        </form>
    )
}

function RegisterForm({ user }) {
    const [data, setData] = React.useState({})
    const [connexionRequest, setConnexionRequest] = React.useState(undefined)
    const [errors, setErrors] = React.useState({})
    const [loading, setLoading] = React.useState(false)
    const handleChange = (field) => (e) => {
        setData((prev) => ({ ...prev, [field]: e.target.value }))
    }

    const handleSubmit = async (e) => {
        setLoading(true)
        const res = await api.post('account/register', { data })
        res.errors && setErrors(res.errors)
        res.data && setConnexionRequest(res.data)
        setLoading(false)
    }

    return (
        <Card
            sx={{
                position: 'absolute',
                top: '50%',
                left: '50%',
                transform: 'translate3d(-50%, -50%, 0)',
                minWidth: { xs: '100%', sm: '90%', md: '60%' },
            }}
        >
            {connexionRequest ? (
                <ActivateForm connexionRequest={connexionRequest} />
            ) : (
                <form
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        gap: '1rem',
                        padding: '1rem',
                    }}
                >
                    <CardHeader title="Register" />
                    {errors.global && <Alert severity="error">{errors.global}</Alert>}
                    <TextField
                        error={!!errors.email}
                        helperText={errors.email}
                        label="Email"
                        type="email"
                        name="email"
                        value={data.email || ''}
                        onChange={handleChange('email')}
                        disabled={loading}
                        required
                    />
                    <TextField
                        error={!!errors.password}
                        helperText={errors.password}
                        label="Password"
                        type="password"
                        name="password"
                        value={data.password || ''}
                        onChange={handleChange('password')}
                        disabled={loading}
                        required
                    />
                    <Button variant="contained" onClick={handleSubmit} disabled={loading}>
                        Register
                    </Button>
                </form>
            )}
        </Card>
    )
}

function Main() {
    const [user, setUser] = React.useState(null)

    const handleLogout = async () => {
        setUser(await api.get('account/logout'))
    }

    React.useEffect(() => {
        api.get('account/me').then(({ data, error }) => {
            setUser(data)
        })
    }, [])

    return (
        <Box>
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        2FAstapi
                    </Typography>
                </Toolbar>
            </AppBar>
            {user && user.isAuthenticated ? (
                <Card
                    sx={{
                        position: 'absolute',
                        top: '50%',
                        left: '50%',
                        transform: 'translate3d(-50%, -50%, 0)',
                        minWidth: { xs: '100%', sm: '90%', md: '60%' },
                    }}
                >
                    <Alert severity="success">Welcome {user.email}</Alert>
                    <Button onClick={handleLogout}>Logout</Button>
                </Card>
            ) : (
                <RegisterForm user={user} />
            )}
        </Box>
    )
}

function Iframe({ src, area }) {
    return (
        <Box
            sx={{
                background: 'white',
                gridArea: area,
                width: '100%',
                height: '100%',
                border: 'none',
            }}
            component="iframe"
            src={src}
        ></Box>
    )
}

function App() {
    return (
        <Box
            sx={{
                height: '100vh',
                background: 'rgb(122, 121, 121)',
                display: 'grid',
                gridTemplateColumns: '1.2fr 0.8fr',
                gridTemplateRows: '1.2fr 0.8fr',
                gridGap: '0.5rem',
                padding: '0.5rem',
                gridTemplateAreas: "'frontend apidocs'\n 'mailbox apidocs'",
            }}
        >
            <Box
                sx={{
                    background: 'white',
                    gridArea: 'frontend',
                    position: 'relative',
                }}
            >
                <Main />
            </Box>
            <Iframe area="mailbox" src="/mailbox"></Iframe>
            <Iframe area="apidocs" src="/api/v1/docs"></Iframe>
        </Box>
    )
}

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
    <ThemeProvider theme={theme}>
        <CssBaseline />
        <App />
    </ThemeProvider>
)
